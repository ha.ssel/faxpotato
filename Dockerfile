FROM nginx
RUN rm -rf /etc/nginx/sites*
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/
COPY test.conf /etc/nginx/conf.d/
VOLUME /var/log/nginx/log
