# faxpotato
    
                               _
                   .----------/ |<=== floppy disk
                  /           | |
                 /           /| |          _________
                /           / | |         | .-----. |
               /___________/ /| |         |=|     |-|
              [____________]/ | |         |~|_____|~|
              |       ___  |  | |         '-|     |-'
              |      /  _) |  | |           |.....|
     function ======>|.'   |  | |           |     |<=== application
       key    |            |  | |    input  |.....|       software
              |            |  | |            `--._|
       main =>|            |  | |      |
      storage |            |  | ;______|_________________
              |            |  |.' ____\|/_______________ `.
              |            | /|  (______________________)  )<== user
              |____________|/ \___________________________/  interface
              '--||----: `'''''.__                      |
                 || jgs `""";"""-.'-._ <== normal flow  |    central
                 ||         |     `-. `'._of operation /<== processing
         ||      ||         |        `\   '-.         /       unit
       surge     ().-.      |         |      :      /`
     control ==>(_((X))     |      .-.       : <======= output
      device       '-'      \     |   \      ;      |________
         ||                  `\  \|/   '-..-'       / /_\   /|
         ||                   /`-.____             |       / /
         ||                  /  _    /_____________|_     / /_
         ||    peripherals ==>/_\___________________/_\__/ /~ )__
         ||      (hardware) |____________________________|/  ~   )
         ||                                     (__~  ~     ~(~~`
         ||    overflow (input/output error) ===> (_~_  ~  ~_ `)
       .-''-.                                         `--~-' '`
      /______\                              _________
       [____] <=== de-bugging tool       _|`---------`|
                                        (C|           |
                             back-up ===> \           /
      |\ ///|                            `=========`
      | \V// |
      |  |~|  |
      |  |=|  | <=== supplemental data
      |  | |  |
      |  | |  |                          (()____
       \ |=| /              mouse ===>  ('      `\_______,
        \|_|/                            `,,---,,'
    
Faxpotato PoC

## [Goal](https://www.draw.io?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Operation_FaxPotato.drawio#R7V1pc6M4Gv41rtr9YArd0sccfdX2kW137cx8miJGdqgQ8ADuJPPrVzLIBoExxsaxd5uqmbYkzvd99LyHjozQzdPLh8RbPHyJfRmOoOu%2FjNDtCKrDpeofXfNa1AAO85p5Evh5HdhUTIK%2FZVHpFrXLwJdp5cQsjsMsWFQrp3EUyWlWqfOSJH6unjaLw%2BpTF95c1iomUy%2Bs1%2F4W%2BNlDXssh29R%2FlMH8wTwZUJG3PHnm5OJL0gfPj59LVejdCN0kcZzlv55ebmSopWfkkl%2F3fkvr%2BsUSGWVdLpiKr399%2FHb%2FXdx%2B5Y%2FjL%2FA%2Fdx%2F%2FHINCPz%2B9cFl8cfG22asRQZYEXjTXpevnhyCTk4U31U3PSuWq7iF7ClUJqJ9JnHlZEEeqOAYMq5pZEIY3cRgnq1shCXwimapPsyR%2BlKUWQRnyqGqpf1bxpT9lksmXUlXxmR9k%2FCSz5FWdYmBnwFOAjqCi%2FLzRIDJqeShpD5kTvQI18%2FW9N4JVPwrZNstZEjS5%2BvLb18k0%2BPZ8%2F%2Fyvd%2FNvv48Z2y1nGflXGrGqFMWRrIpWSSN5%2FV0VXIeY4h%2Fltlv98e669FqUtooz85K5zNpemeQnSr%2FSQ%2BpSL0mVNAjV1CUyVOD4We1XTYIunnAXB%2BqV10rlLnBIRa2AWNpK42UylcV15Z5g34rUbuVat8qlU7vVSvfrT%2B8PB4R2w2GexMuF3amI7iJ%2BkCiqy6vUN2vh79Ft1lzp3ZtHuc2KNVe5vCIsxOvdCeMGzWM%2BVHfCJ2UtxVncx02sxeE9os2s1a73zlzGgQVU%2BOZUdlrZe5LPpk2yp1Mu72dDyp5UZU%2Fe3Irwk4p%2Bxqdy2ij6e04wabYuRxI9RBXRmy9%2FO9EDUZP95OPV99vx7c1EVRPsqCe%2Fh7hu1cNQ%2BapaH8oBXOjKaRgv%2Fd36GUy2Y2D5RwCwmngBaxDvYHwO6sjOpTv5l64m5HKkCxmsWktQpw1A%2BSmFW2dsA92PWrjoBMLd24dHtIpR1ODDA35KjGKym32NnJ5e5joedqZBOo2dhYwXoXS8yE%2FiuYziZfrnQiappt6y1Bba25TJu59KamlR53vpg%2FQLX97iZxdRLhqDqdnqWLeYwFWJ%2B1prIFDh7WfvXoZ3cRoUJuA%2BzrL4qXTCVRjMdUMWa%2BV6RWkq9SuqCuV2hkGkHmrC7tZgo7veAbbIiTX0HxNjV%2FQOh9J7PUS%2Buvs0njx4iZ%2Bq%2Bn8AgRzAVUgB844E%2FzmCNFSvdX2fqF%2FzbCUZuyaV7thbBE6WLNMsfHWmSvqqM0JHUcN7pB%2Fo%2B3E6TvVjmq4GTVej0tXTh%2B0Xw6aLcfniliejpovJ5uL0EZqLbedEvmSWEyLT4O8iEHFNH1hpkFyPyK1G3jJTKF2lhUAJiKGcZVvRmipmCqL5D124HeMjIZPalARJDZmwySsRg1FS3Wzeymwl8zjxdMPoBo2ucBL6afyULvJSFCkmkqo1ysuZksFjuLru0QtVkD7SH0H%2FWurM1PWKkyLdamrOyk7YjiLCdboQnNWVQuBQWiFwt6Go53rWTA%2B2Jn7QuvzHqsx35H5WpTuZBOq7NGnnlS9Blt%2BOQlKU89vBorS5my68lgr2vbbqL8%2FEdPBJdmahwHlloahlnajxCvbNQVHXIhM7mTVwBop0yED1AqmgVZCyw0DKeRWj5JQg5ZcJUoKgA4hiQiYAY8BKISFCHVeomI9jDDCltCeAoXCEuzmsh5hw4lRw7pAR7AVnl1XhjA%2BDM66gGZwSzOJSwcwcjrfhjNlWvDN8USt80Ynh22EYrgTfaeilaZCn5rwkq1eXsXxEKi0PQpFqvxCkT8c4Rsbx6GxuvOpz6QDccjnsDEd3yFs3spOlA4OcGud%2BA%2FIPP36MfyRelAZZDe%2B9gkU7TWHHh0%2BB76%2BS46HOf1x700c9zBb5TcmTOtTa%2B%2B7BA21jZW4EqQY04DAMmVPi2SyVw1DXfiPbe1HXkUOf%2FdlmQ565H7AmT7w3dx5AXF1jpTMjLjtWwvYge1fiWr%2BQmURk4vlTEZdbg%2Finu%2FG3KHy9fPIyvfdg8lJ9glheFDx77uowoHo4dwFkRcS9QuIjukqXRiSs2v1JXx4B0JrIgk%2Bcc6mPIX%2BWP2WILp9GTEc6Ao0ADqxg7NxphB414bu1255Jb6SsOneOEdfpmQTl1ohKw62GNu117%2FXD93cX2w%2FXSDxCP3Rdk1Q74563Xxb7rfImR7TdZ0ICnOK2rByzbtg5RWFzCwOnJoR6IvnSjTPdNofpHBIU5s6ictMxrN5gOAYB9Vk9195sFkTqbrlzpn7cxLHqv08LL9LCWM%2FzqFPNGw6EE2tAEYD67ATUxAhguJnC%2B2W1uy962IRMWwKo8xg%2BLBRwcSstrHRL73UWwobkiVdZsHrf%2FhQFWeCFKyH9tZSpfttAz%2BKSQfagdK7eTf2HXl7U%2F2NTXHivYez5XWZ1IXcFDWt2VianDw1Xs2v9gEUwQleb2TVbrl3PvXFH7LbLm9zE0SyYj%2BDN6luzZRKNVtN6rNN0Hclfua0RtjWi7Y3ec7q9cZ6zY63dlzNvGa7mMUmzeK6JZIcnVYFtBNc5lTb0RDgcp9ZzCEaMZ2SMxvb03fE6iVPmsMZFQzZJHG%2F1RD2Nu4LnGcmNWdQ7xvTNxWZWwewdYzUHU6JqkPcfSCk5BevJGKXpFPvmVWsx2Pv3OqA5yPLjrpb%2FzOZaAAyrfjhlPU0%2FtQatAe1m%2BhWKvNfSaUUE1fLGrBp6js3Cla1vZlG6fYH6kb%2FEccMMt%2B4Lq7guC6L5eO2JXGh8SbbRXI%2BkE8LgwKTTepFT9YoBB5HavMzvay9TP1eVc52XW6j3pA1JdJ8uGh25acM6AfucibrnaueFlc%2F676V2Vt0rDWV11E%2F%2F8O5H%2Fkb3cZwpAvQWf%2F9Cn85uUIwtt%2BUoYDRZZkOGdk5rOHDyviOcjcYborLxPsRyH2VaZc10u6423icy3ezMTDcHDnbF5qhAjiHhULZp5P3MOrH8U2wvbTmaWYe02hFxu1VHDLWdP4xRF%2FWAYiJV%2F4l1qJ3Fo9WeL4mfx%2BDeLFvlF%2F5XrD4%2F2pCv4l3ESJV4kXv2o77ArRt%2BFTcu%2FXicKuE%2FvkYKC78U3z7GiNFxxg%2BG8PYaN0zCnXdu6ZwN2FtuwgquCBWOwMr6EcABwmYBejlJ5UIHlo%2B6HcIEOABBADEiECFgRo0OySc0ShAeuhVSg9kvIva1g6EHqlxYdjGAqkB9nIy6HltxUfYbmj%2BfnpXfwKt2EwrkuApPQrkTnBLY008YU%2BQwzhknAjFN5lVvRPkNevEFQ5C6ggJXDLbaolUF5THe71dfJ18%2BTSafvn2tM3O%2FPVX00NP%2B%2B58ZSk9yVW9Z4bwnKjtkIMv7KmCLLVAdfkBQh5bp4nC2aLQ2Dbr6fDWkijps9tS%2BRYJR3PFV9GKcboeXDgGqrhNTzUofLoWAUUBJfeucc9IvaBiA72pQ95Nnj33PsINKhxWSAAodzFTIpRowxLzB7ALBqqqqyxly7igduQi4nLvc5F8HkHM9WBnG7FqznYXYaXY3iyxcswbdXGuG7%2FdMMuw24u1g3JkmgF0Xk8HzShNozLocU4S4K7ggVbuMiHAQZRgylwsqeN%2BlwQ7DgCmLBjDSy0Cqs64hZTrNS6CLCBJ4HfmdaFJAg%2Fv527AGZfcOdn0Nyg5GvVyjX89a3g2qow4zMt9MR%2BvUm0NLB6safQEdFxLhKi%2BSMLJe6HCm%2BlUe%2F3DGqLAmoGJJdsz7MgbMWmW4a03zL7NioMtb0QmQcLRv4wJCMHZxz9nBQOC2pzDqkNJDXMuVGtiuALNqZXAHy1p5r763T17D9JPVLWmps2yeceA8yUP7Qtdlr%2FC8VquNeWvYQFgrSjtnWNqfAln1MdZXDJxgQQOm%2BEqwBRZqwf47WLVkDHVFrxlF9Z7QGqiWO0Jrump3Rziv1OJYZyIwosiFnApCuBVsCAcoV0W1KPOBBLO3XOmea6xNV25LYQ4N%2Ff0G3HtZgT29lD4e0S8MFxdYa4%2B5ha2enoxyrh1XgV8pFzNKsKhOEIGIOxwQCpAKnpVTDk%2B73wEA8P8dxK3YvDzvvMq8QO8Ad%2FggD7TneLITEe8W0Pba0XBvp%2BNYKcq3HaY8ovd9XoRtYRK1Y7Ir1C0rgODbIr3XZoeHIt1d%2B9t9kW6NDJQi2TdF%2BqWyuhjENWHWKBcXb%2BhN4yak57OZZ%2FHqrTeQN0vkdMM43zH8Sp0AxOKlvH4unwH9XfpL34umr%2BPP3qtM1ovU1FvmNzbL6uyU8yXvYF6bN9T0pyma%2Fu4H3H9tkSpu%2FrRdDofNXwhE7%2F4L) 

Provide a simpel way to handle an initial routing request, via 3xx, staitc-payload, or backend-selector.
### Pseudo-exampel

```
---
shards:
  - name: eu-shard-0
    fallback: api.mooo.com
    strategy: 3xx, upstream, payload
    sites:
      - eu0-api.mooo.tech
      - eu1-api.mooo.tech
      - eu2-api.mooo.tech
      - eu-gcp.mooo.tech
      - eu-aws.mooo.tech
```
### Should result in config:

```
    split_clients "${binary_remote_addr}" $site {
      20%   eu0-api.mooo.tech;
      20%   eu1-api.mooo.tech;
      20%   eu2-api.mooo.tech;
      20%   eu-gcp.mooo.tech;
      20%   eu-aws.mooo.tech;
      *     api.mooo.tech;
    }
```
```
      location / {
        return 200
          '{"endpoint":"$scheme://$site"}';
      }
```
```
      location / {
        return 301
          '$scheme://$site"';
  }
```
```
    location / {
        status_zone faxpotato.mooo.tech-L7;
        proxy_pass $scheme://$site.upstream;
        health_check match=default mandatory;
  }
```
